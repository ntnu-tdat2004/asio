#include <asio.hpp>
#include <iostream>
#include <thread>

using namespace std;

int main() {
  static asio::io_context workers(4);    // Create 4 workers, optimized for running in 4 threads
  static asio::io_context event_loop(1); // Create event_loop optimized for running in one thread

  for (size_t c = 0; c < 16; ++c) {
    asio::post(workers, [c] {     // Add task to workers
      this_thread::sleep_for(1s); // Simulate heavy work
      string result = "result " + to_string(c);

      asio::post(event_loop, [result = std::move(result)] { // Send result to event_loop
        cout << result << endl;
      });
    });
  }

  auto workers_guard = asio::make_work_guard(workers); // Keep the workers working
  for (size_t c = 0; c < 4; ++c) {                     // Run workers in separate threads
    thread thread([] {
      workers.run();
    });
    thread.detach(); // Let the thread run in the background
  }

  auto event_loop_guard = asio::make_work_guard(event_loop); // Keep the event_loop working
  event_loop.run();                                          // Run event_loop in main thread
}

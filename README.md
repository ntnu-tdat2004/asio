# Asynchronous IO examples

## Recommended
* Linux or MacOS
* The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp)

## Installing dependencies

### Debian based distributions
`sudo apt-get install libasio-dev`

### Arch Linux based distributions
`sudo pacman -S asio`

### MacOS
`brew install asio`


## Compiling and running
```sh
git clone https://gitlab.com/ntnu-tdat2004/asio
juci asio
```

Choose Compile and Run in the Project menu.
